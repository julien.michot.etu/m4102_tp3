package fr.ulille.iut.pizzaland.dto;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaDto {
	private long id;
	private String name;
	private double prixpetite;
	private double prixgrande;
	private Ingredient[] ingredients;

	public PizzaDto() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public double getPrixpetite() {
		return prixpetite;
	}

	public void setPrixpetite(double prixpetite) {
		this.prixpetite = prixpetite;
	}

	public double getPrixgrande() {
		return prixgrande;
	}

	public void setPrixgrande(double prixgrande) {
		this.prixgrande = prixgrande;
	}

	public Ingredient[] getIngredients() {
		return ingredients;
	}

	public void setIngredients(Ingredient[] ingredients) {
		this.ingredients = ingredients;
	}
}
