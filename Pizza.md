# Pizza

## Développement d'une ressource *Pizza*

### API et représentation des données

Nous pouvons tout d'abord réfléchir à l'API REST que nous allons offrir pour la ressource *pizza*. Celle-ci devrait aux URI suivantes :

| Opération | URI               | Action réalisée                          | Retour                                              |
|:----------|:------------------|:-----------------------------------------|:----------------------------------------------------|
| GET       | /pizzas           | récupère l'ensemble des pizzas           | 200 et un tableau de pizzas                         |
| GET       | /pizzas/{id}      | récupère la pizza d'id ID                | 200 et la pizza                                     |
|           |                   |                                          | 404 si la pizza n'existe pas                        |
| GET       | /pizzas/{id}/name | récupère le nom de la pizza avec id      | 200 et le nom de la pizza                           |
|           |                   |                                          | 404 si la pizza n'existe pas                        |
| POST      | /pizzas           | création d'une pizza                     | 201 et l'URI de la ressource créée + représentation |
|           |                   |                                          | 400 si les informations ne sont pas correctes       |
|           |                   |                                          | 400 si la pizza existe déjà (même nom)              |
| DELETE    | /pizzas/{id}      | destruction de la pizza d'identifiant id | 200 et un tableau de pizzas                         | 
|           |                   |                                          | 404 si l'id est inconnu                             |

Une pizza comporte un identifiant, un nom et une liste d'ingrédients (id des ingrédients). Sa représentation JSON prendra donc la forme suivante :

{
    "id": 2,
    "name": "regina",
    "ingredients": [1, 6],
    "prixpetite": 5,
    "prixgrande": 12
}

Lors de la création, l'indentifiant n'est pas connu car il sera fourni par la base de données.

{ "name": "regina", "ingredients": [1, 6], "prixpetite": 5, "prixgrande": 12 }